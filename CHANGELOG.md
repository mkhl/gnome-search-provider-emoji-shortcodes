# Change Log

All notable changes to the "direnv" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Unreleased]

## [0.1.1] - 2022-10-18
- RPM packaging

## [0.1.0] - 2022-10-15
- Initial release

[Unreleased]: https://codeberg.org/mkhl/gnome-search-provider-emoji-shortcodes/compare/v0.1.1...HEAD
[0.1.1]: https://codeberg.org/mkhl/gnome-search-provider-emoji-shortcodes/compare/v0.1.0...v0.1.1
[0.1.0]: https://codeberg.org/mkhl/gnome-search-provider-emoji-shortcodes/releases/tag/v0.1.0
