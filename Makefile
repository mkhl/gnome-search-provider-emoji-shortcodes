.POSIX:

DESTDIR =
PREFIX 	= /usr/local

DATADIR     = $(PREFIX)/share
LIBEXECDIR  = $(PREFIX)/libexec
USERUNITDIR = $(PREFIX)/lib/systemd/user

CARGO = cargo

NAME  = gnome-search-provider-emoji-shortcodes
QNAME = org.codeberg.mkhl.search-provider.emoji-shortcodes

GENERATED = target/dbus/$(QNAME).service target/systemd/$(QNAME).service

all: $(GENERATED) ## build all artifacts [default]
	$(CARGO) build --release --locked

target/release/$(NAME):
	$(CARGO) build --release --locked

target/dbus/$(QNAME).service: dbus/$(QNAME).service
	@mkdir -p target/dbus/
	sed "s:@libexecdir@:$(LIBEXECDIR):g" dbus/$(QNAME).service > target/dbus/$(QNAME).service

target/systemd/$(QNAME).service: systemd/$(QNAME).service
	@mkdir -p target/systemd/
	sed "s:@libexecdir@:$(LIBEXECDIR):g" systemd/$(QNAME).service > target/systemd/$(QNAME).service

clean: ## remove the built artifacts
	rm -rf target/

install: $(GENERATED) ## install the artifacts
	@mkdir -p $(DESTDIR)$(LIBEXECDIR)/
	cp target/release/$(NAME) $(DESTDIR)$(LIBEXECDIR)/
	@mkdir -p $(DESTDIR)$(DATADIR)/$(NAME)/
	cp emoji.json $(DESTDIR)$(DATADIR)/$(NAME)/
	@mkdir -p $(DESTDIR)$(USERUNITDIR)/
	cp target/systemd/$(QNAME).service $(DESTDIR)$(USERUNITDIR)/
	@mkdir -p $(DESTDIR)$(DATADIR)/dbus-1/services/
	cp target/dbus/$(QNAME).service $(DESTDIR)$(DATADIR)/dbus-1/services/
	@mkdir -p $(DESTDIR)$(DATADIR)/applications/
	cp gnome/$(QNAME).desktop $(DESTDIR)$(DATADIR)/applications/
	@mkdir -p $(DESTDIR)$(DATADIR)/icons/
	cp gnome/$(QNAME).png $(DESTDIR)$(DATADIR)/icons/
	@mkdir -p $(DESTDIR)$(DATADIR)/gnome-shell/search-providers/
	cp gnome/$(QNAME).ini $(DESTDIR)$(DATADIR)/gnome-shell/search-providers/

uninstall: ## uninstall the artifacts
	-rm -f $(DESTDIR)$(LIBEXECDIR)/$(NAME)
	-rm -f $(DESTDIR)$(USERUNITDIR)/$(QNAME).service
	-rm -f $(DESTDIR)$(DATADIR)/$(NAME)/emoji.json
	-rm -f $(DESTDIR)$(DATADIR)/dbus-1/services/$(QNAME).service
	-rm -f $(DESTDIR)$(DATADIR)/applications/$(QNAME).desktop
	-rm -f $(DESTDIR)$(DATADIR)/icons/$(QNAME).png
	-rm -f $(DESTDIR)$(DATADIR)/gnome-shell/search-providers/$(QNAME).ini

help: ## show this help
	@awk -F ':.* ## ' '/^\w.*##/{printf "%-15s %s\n", $$1, $$2}' Makefile
