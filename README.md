# [GNOME Search Provider][gnome-search-provider] for [Emoji Shortcodes][emoji-shortcodes]

Because I wanted emoji in non-GTK applications
with the shortcodes I'm used to!

## Which shortcodes?

The ones supported by [node-emoji][].
I might switch at some point
but only to a library that includes the emoji in the data 😒

## What's that logo?

It's an off-brand octocat!
You get it by combining octopus and cat in [Emoji Kitchen][emoji-kitchen]!

## How do you install it?

### Arch Linux (btw)

Install [from the AUR][aur].

### Fedora

Install [from Copr][copr]:

	dnf copr enable mkhl/gnomish
	dnf install gnome-search-provider-emoji-shortcodes

### From Source

You'll need:

* GNOME
* Rust, Cargo etc.
* Make
* Sed

Build it with:

	make

Install it with:

	sudo make install

[Installing under `$HOME` **will not work**.][nohome]

[aur]: https://aur.archlinux.org/packages/gnome-search-provider-emoji-shortcodes
[copr]: https://copr.fedorainfracloud.org/coprs/mkhl/gnomish/
[emoji-kitchen]: https://emoji.supply/kitchen/
[emoji-shortcodes]: https://emojipedia.org/shortcodes/
[gnome-search-provider]: https://developer.gnome.org/documentation/tutorials/search-provider.html
[node-emoji]: https://github.com/omnidan/node-emoji
[nohome]: https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/3060
