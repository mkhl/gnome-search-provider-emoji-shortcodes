Name:           {{{ git_dir_name }}}
Version:        {{{ git_dir_version }}}
Release:        1%{?dist}
Summary:        GNOME Search Provider for Emoji Shortcodes

License:        0BSD
URL:            https://codeberg.org/mkhl/%{name}
VCS:            {{{ git_dir_vcs }}}
Source:         {{{ git_dir_pack }}}

BuildRequires:  systemd-rpm-macros
BuildRequires:  cargo
BuildRequires:  make
BuildRequires:  sed
Requires:       gnome-shell
Requires:       systemd

%description

%define _debugsource_template %{nil}
%define qname org.codeberg.mkhl.search-provider.emoji-shortcodes

%prep
{{{ git_dir_setup_macro }}}
cargo fetch --locked --target "%{_arch}-unknown-linux-gnu"

%build
export RUSTUP_TOOLCHAIN=stable
export CARGO_TARGET_DIR=target
cargo build --frozen --release --all-features
make CARGO=true PREFIX="%{_prefix}" LIBEXECDIR="%{_libexecdir}" USERUNITDIR="%{_userunitdir}"

%install
make CARGO=true DESTDIR="%{buildroot}" PREFIX="%{_prefix}" LIBEXECDIR="%{_libexecdir}" USERUNITDIR="%{_userunitdir}" install

%if %{with check}
%check
cargo test --frozen --all-features
%endif

%files
%license LICENSE
%doc README.md
%{_libexecdir}/%{name}
%{_userunitdir}/%{qname}.service
%{_datadir}/applications/%{qname}.desktop
%{_datadir}/icons/%{qname}.png
%{_datadir}/dbus-1/services/%{qname}.service
%{_datadir}/gnome-shell/search-providers/%{qname}.ini
%{_datadir}/%{name}/emoji.json

%changelog
* Tue Oct 18 2022 Martin Kühl <martin.kuehl@posteo.net> - 0.1.1-1
- Initial package
