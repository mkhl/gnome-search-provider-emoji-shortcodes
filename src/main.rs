use std::{collections::BTreeMap, error::Error, fs};

use futures_executor::block_on;
use search_provider::{ResultID, ResultMeta, SearchProvider, SearchProviderImpl};

const THIS: &str = "gnome-search-provider-emoji-shortcodes";
const NAME: &str = "org.codeberg.mkhl.SearchProvider.EmojiShortcodes";
const PATH: &str = "/org/codeberg/mkhl/SearchProvider/EmojiShortcodes";
const FILE: &str = "emoji.json";

type Emoji = BTreeMap<String, String>;
type Result<T> = core::result::Result<T, Box<dyn Error>>;

struct Application {
    emoji: Emoji,
}

impl SearchProviderImpl for Application {
    fn activate_result(&self, _identifier: ResultID, _terms: &[String], _timestamp: u32) {}

    fn initial_result_set(&self, terms: &[String]) -> Vec<ResultID> {
        let previous_results = self
            .emoji
            .keys()
            .map(Into::into)
            .collect::<Vec<String>>();
        self.subsearch_result_set(&previous_results, terms)
    }

    fn subsearch_result_set(
        &self,
        previous_results: &[ResultID],
        terms: &[String],
    ) -> Vec<ResultID> {
        previous_results
            .iter()
            .filter(|key| terms.iter().all(|term| key.contains(term.trim_start_matches(':'))))
            .map(Into::into)
            .collect()
    }

    fn result_metas(&self, identifiers: &[ResultID]) -> Vec<ResultMeta> {
        identifiers
            .iter()
            .map(|identifier| {
                let emoji = self.emoji.get(identifier).unwrap();
                ResultMeta::builder(identifier.into(), emoji)
                    .description(identifier)
                    .clipboard_text(emoji)
                    .build()
            })
            .collect()
    }
}

fn emoji() -> Result<Emoji> {
    let dirs = xdg::BaseDirectories::with_prefix(THIS)?;
    let path = dirs.find_data_file(FILE).unwrap_or_else(|| FILE.into());
    let data = fs::read(path)?;
    let json = serde_json::from_slice(&data)?;
    Ok(json)
}

async fn run() -> Result<()> {
    let emoji = emoji()?;
    let app = Application { emoji };
    SearchProvider::new(app, NAME, PATH).await?;
    Ok(())
}

fn main() -> Result<()> {
    block_on(run())
}
